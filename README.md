<p align="center">
    <a href="https://sylius.com" target="_blank">
        <img src="https://demo.sylius.com/assets/shop/img/logo.png" />
    </a>
</p>

<h1 align="center">Sylius Fundamentals</h1>

<p align="center">Practical code examples developed during the "Sylius Fundamentals" video course</p>

